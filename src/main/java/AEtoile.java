import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;

public class AEtoile {



    public static LinkedList<City> smallestWay(LinkedList<City> graph, City source, City destination) {
        LinkedHashSet<City> closedSet = new LinkedHashSet<>();
        LinkedList<City> openSet = new LinkedList<>();
        openSet.add(source);
        LinkedHashMap<City, City> cameFrom = new LinkedHashMap<>();
        LinkedHashMap<City, Double> gScore = new LinkedHashMap<>();
        gScore.put(source, (double) 0);
        LinkedHashMap<City, Double> fScore = new LinkedHashMap<>();
        graph.forEach(city -> fScore.put(city, Double.MAX_VALUE));
        fScore.put(source, source.distanceTo(destination));

        Comparator<City> comparator = (o1, o2) -> {
            if (fScore.get(o1) < fScore.get(o2)) {
                return -1;
            }
            if (fScore.get(o2) < fScore.get(o1)) {
                return 1;
            }
            return 0;
        };

        while (!openSet.isEmpty()) {
            City current = openSet.get(0);
            System.out.println("Current : " + current.getId());
            if (current.getId().equals(destination.getId())) return reconstructPath(cameFrom, destination); // TODO: Reconstruire la route
            openSet.remove(0);
            closedSet.add(current);
            for (Road road : current.getSuccessor()) {
                City neighbor = road.getCityDestination();
                if (closedSet.contains(neighbor)) {
                    continue;
                }
                double tmpGScore = gScore.get(current) + current.distanceTo(neighbor);
                if (!openSet.contains(neighbor)) {
                    openSet.add(neighbor);
                }else if ((tmpGScore >= gScore.get(neighbor))) continue;
                cameFrom.put(neighbor, current);
                gScore.put(neighbor, tmpGScore);
                double estimatedFScore = gScore.get(neighbor) + neighbor.distanceTo(destination);
                fScore.put(neighbor, estimatedFScore);
                openSet.sort(comparator);
            }
        }
        return null;
    }

    private static LinkedList<City> reconstructPath(LinkedHashMap<City, City> cameFrom, City current) {
        LinkedList<City> totalPath = new LinkedList<>();
        totalPath.add(current);
        while (current != null) {
            City previous = current;
            current = cameFrom.get(current);
            if (current != null) {
                totalPath.add(current);
            }
        }
        return totalPath;
    }
}
